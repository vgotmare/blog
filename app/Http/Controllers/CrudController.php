<?php

namespace App\Http\Controllers;

use App\Crud;
use Illuminate\Http\Request;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees=Crud::all();
        return view('listemployee')->with('employees',$employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addemployee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = new Crud();
        $employee-> name = $request->get('name');
        $employee-> Address = $request->get('address');
        $employee-> Email = $request->get('email');
        $employee-> ContactNo = $request->get('contactno');
        $employee->save();
        return redirect('addemployee');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Crud  $crud
     * @return \Illuminate\Http\Response
     */
    public function show(Crud $crud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Crud  $crud
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            //echo "$id";
        $employee=Crud::find($id);
       // print_r($employee);
        return view('editemployee',compact('employee','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Crud  $crud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // $employee = new Crud();
       $employee=Crud::find($id);
        $employee-> name = $request->get('name');
        $employee-> Address = $request->get('address');
        $employee-> Email = $request->get('email');
        $employee-> ContactNo = $request->get('contactno');
        $employee->save();
        return redirect('addemployee');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Crud  $crud
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $employee=Crud::find($id);
       // echo $id;
       $employee->delete();
       return redirect('addemployee');
    }
}

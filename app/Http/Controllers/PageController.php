<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    function Service(){
        return view('Page.Service');
    }

    function Document(){
        return view('Page.Document');
    }
    function Contactus(){
        return view('Page.Contactus');
    }
    function Aboutus(){
        return view('Page.Aboutus');
    }
}

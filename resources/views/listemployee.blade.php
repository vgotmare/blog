@extends('layouts.app')

@section('content')

<div class="container">
<a href={{url('addemployee/create')}}><button>Add Employee</button></a>
<hr>
<table class="table table-striped">
    <tr>
        <td>
            Id
        </td>
    
    
        <td>
            Name
        </td>
    
    
        <td>
            Address
        </td>
    
    
        <td>
            Email
        </td>
    
    
        <td>
            Contact No
        </td>
        <td colspan="2" align="center">
            Action
        </td>
    </tr>
@foreach ($employees as $employee) 
    <tr>
        <td>{{$employee['id']}}</td>
        <td>{{$employee['name']}}</td>
        <td>{{$employee['Address']}}</td>
        <td>{{$employee['Email']}}</td>
        <td>{{$employee['ContactNo']}}</td>
        <td align="right"><a href="{{action('CrudController@edit',$employee['id'])}}" class="btn btn-success">Edit</a></td>
        <td align="left">
            <form action="{{action('CrudController@destroy',$employee['id'])}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
            </form>
        </td>
</tr>
@endforeach
</table>
</div>
@endsection

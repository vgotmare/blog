@extends('layouts.app')

@section('content')
<a href="{{action('CrudController@update',$employee->id)}}"><button>Make CRUD</button></a>
<form action="{{action('CrudController@update',$employee->id)}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input name="_method" type="hidden" value="PATCH">
    <table >


        <tr>
            <td>
                Name
            </td>
            <td>
                <input type='text' name="name" value="{{$employee->name}}">
            </td>

        </tr>
        <tr>
            <td>
                Address
            </td>
            <td>
                <input type='text' name="address" value="{{$employee['Address']}}">
            </td>

        </tr>
        <tr>
            <td>
                Email
            </td>
            <td>
                <input type='text' name="email" value="{{$employee['Email']}}">
            </td>

        </tr>
        <tr>
            <td>
                Contact No
            </td>
            <td>
                <input type='text' name="contactno" value="{{$employee['ContactNo']}}">
            </td>

        </tr>
        <tr>
            <td>
            </td>
            <td>
                <input type='submit' value="Update Employee" >
            </td>

        </tr>
    </table>


</form>





@endsection

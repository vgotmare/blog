@extends('layouts.app')

@section('content')

<form action="{{url('addemployee')}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <table >
        <tr>
            <td>
                Name
            </td>
            <td>
                <input type='text' name="name">
            </td>

        </tr>
        <tr>
            <td>
                Address
            </td>
            <td>
                <input type='text' name="address">
            </td>

        </tr>
        <tr>
            <td>
                Email
            </td>
            <td>
                <input type='text' name="email">
            </td>

        </tr>
        <tr>
            <td>
                Contact No
            </td>
            <td>
                <input type='text' name="contactno">
            </td>

        </tr>
        <tr>
            <td>
            </td>
            <td>
                <input type='submit' value="Add Employee">
            </td>

        </tr>

    </table>


</form>





@endsection
